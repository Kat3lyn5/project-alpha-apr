from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import Taskform
from tasks.models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = Taskform(request.POST)
        if form.is_valid():
            task = form.save()
            task.owner = request.user
            task.save()
            return redirect("list_projects")
    else:
        form = Taskform()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "my_tasks_object": tasks,
    }
    return render(request, "tasks/list.html", context)
